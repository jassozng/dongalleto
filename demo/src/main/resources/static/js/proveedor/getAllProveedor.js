export default function getAllProveedor(){
    let data = {}
    //Ajax request.
    $.ajax({
        method:"GET",
        url: "/rest/proveedor/getAll",
        async:true,
        data:data
    }).done(function(data){
            var result = '';
            result = JSON.parse(data);
            //If there's an exception...
            if(result.exception != null){
                //Check if exception is no data in db.
                if(result.exception == "No data"){
                    $('#provTable > tbody').append('<tr>'+
                    '<td class="font-weight-bold" colspan="4">No se encontraron registros :(</td>'+
                    '</tr>');
                }
                //Other kind exception.
                else{
                    Swal.fire("La consulta no se pudo procesar",
                    result.exception,
                    "error");
                }
            }
            //If everything is fine..
            else{
                let records = [];
                records = result;
                for(let i=0; i<records.length; i++){
                    $('#provTable > tbody').append('<tr>'+
                    '<td class="font-weight-bold"><a id="prov'+i+'">'+records[i].companyName+'<a></td>'+
                    '<td>'+records[i].street+' #'+records[i].num+', '+records[i].suburb+'</td>'+
                    '<td>'+records[i].tel+'</td>'+
                    '<td>'+records[i].managerName+' '+records[i].managerLastNamep+' '+records[i].managerLastNamem+'</td>'+
                    '</tr>');
                    $('#prov'+i+'').click(function(){
                        getRecord(records[i]);
                    });
                }
            }
        });        
}

function getRecord(record){
    $("#idProveedor").val(record.idProveedor);
    $("#inputCompanyName").val(record.companyName);
    $("#inputStreet").val(record.street);
    $("#inputNum").val(record.num);
    $("#inputSuburb").val(record.suburb);
    $("#inputTel").val(record.tel);
    $("#inputManagerName").val(record.managerName);
    $("#inputLastName1").val(record.managerLastNamep);
    $("#inputLastName2").val(record.managerLastNamem);
}