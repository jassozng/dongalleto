import getAllProveedor from "./getAllProveedor.js";

export default function insertProveedor(){
    let idProveedor = document.getElementById("idProveedor").value;
    let companyName = document.getElementById("inputCompanyName").value;
    let street = document.getElementById("inputStreet").value;
    let num = document.getElementById("inputNum").value;
    let suburb = document.getElementById("inputSuburb").value;
    let tel = document.getElementById("inputTel").value;
    let managerName = document.getElementById("inputManagerName").value;
    let managerLastNamep = document.getElementById("inputLastName1").value;
    let managerLastNamem = document.getElementById("inputLastName2").value;

    let data = {
        idProveedor:idProveedor,
        companyName:companyName,
        street:street,
        num:num,
        suburb:suburb,
        tel:tel,
        managerName:managerName,
        managerLastNamep:managerLastNamep,
        managerLastNamem:managerLastNamem
    }

    //This iteration validate if any value is empty or select element has an invalid value.
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            if(data[key] === ""){
                Swal.fire("La consulta no se pudo procesar",
                "Por favor, llene todos los campos",
                "error");
                return;
            }
        }
    }

    //Ajax request.
    $.ajax({
        method: "POST",
        url: "/rest/proveedor/insert",
        data: data,
        async: true
    }).done(function (data) {
        var result = '';
        result = JSON.parse(data);
        //If there is any exception, this alert will show it.
        if (result.exception != null) {
            Swal.fire("La consulta no se pudo procesar",
            result.exception,
            "error");
        }
        //If not, this alert show a confirmation message.
        else {
            Swal.fire("Proveedor registrado con exito",
            result.name,
            "success");
            clearFields();
            $("#provTable > tbody > tr").remove();
            getAllProveedor();
        }
    });
}

function clearFields(){
    $("#idProveedor").val("0");
    $("#inputCompanyName").val("");
    $("#inputStreet").val("");
    $("#inputNum").val("");
    $("#inputSuburb").val("");
    $("#inputTel").val("");
    $("#inputManagerName").val("");
    $("#inputLastName1").val("");
    $("#inputLastName2").val("");
}