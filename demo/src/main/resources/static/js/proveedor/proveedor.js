import getAllProveedor from "./getAllProveedor.js";
import insertProveedor from "./insertProveedor.js";
import deleteProveedor from "./deleteProveedor.js";

window.onload = function () {
    getAllProveedor();
    let deleteButton = document.getElementById("btnDelete");
    deleteButton.addEventListener("click", deleteProveedor);
    let updateButton = document.getElementById("btnUpdate");
    updateButton.addEventListener("click", insertProveedor);
    let insertButton = document.getElementById("btnInsert");
    insertButton.addEventListener("click", insertProveedor);
}