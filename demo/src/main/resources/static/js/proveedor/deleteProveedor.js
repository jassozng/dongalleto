import getAllProveedor from "./getAllProveedor.js";

export default function deleteProveedor(){
    let idProveedor = document.getElementById("idProveedor").value;
    
    if(idProveedor == "0"){
        Swal.fire("La consulta no se pudo procesar",
                    "Por favor, seleccione un proveedor.",
                    "error");
    }else{

        let data = {
            idProveedor:idProveedor
        }
        
        Swal.fire({
            title: 'Confirmación',
            text: "¿Está seguro de querer eliminar este administrador?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
          }).then((result) => {
            if (result.value) {
                //Ajax request.
                $.ajax({
                    type:"POST",
                    url: "/rest/proveedor/delete",
                    async:true,
                    data:data
                }).done(function(data){
                    var result = '';
                    result = JSON.parse(data);
                    //If there's an exception...
                    if(result.exception != null){
                        Swal.fire("La consulta no se pudo procesar",
                        result.exception,
                        "error");
                    }else{
                        Swal.fire("Eliminación exitosa",
                        "",
                        "success");
                        $("#provTable > tbody > tr").remove();
                        getAllProveedor();
                    }
                });
            }
        });   
    }    
}