package com.utl.idgs.dongalleto.demo.controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.utl.idgs.dongalleto.demo.model.proveedor;
import com.utl.idgs.dongalleto.demo.database.conexionMySQL;

public class controllerProveedor {
    public int insert(proveedor prov) throws Exception{
        //Generate the call to the procedure
        String query="{CALL insertProveedor(?,?,?,?,?,?,?,?," +     //Proveedor data
                                                         "?)}";    //Return values
        //Generate the connection and open it
        conexionMySQL connMySQL = new conexionMySQL();
        Connection conn = connMySQL.open();
        //Generate the statement for query
        CallableStatement cstmt = conn.prepareCall(query);
        //Fill Admin data according to the procedure parameters.
        cstmt.setString(1, prov.getCompanyName());
        cstmt.setString(2, prov.getStreet());
        cstmt.setString(3, prov.getNum());
        cstmt.setString(4, prov.getSuburb());
        cstmt.setString(5, prov.getTel());
        cstmt.setString(6, prov.getManagerName());
        cstmt.setString(7, prov.getManagerLastNamep());
        cstmt.setString(8, prov.getManagerLastNamem());
        //Register the output parameters.
        cstmt.registerOutParameter(9, Types.INTEGER);
        //Execute the procedure.
        cstmt.execute();
        //Retrieve the generated id
        prov.setIdProveedor(cstmt.getInt(9));
        //Close objects
        cstmt.close();
        connMySQL.close();
        //return new id
        return prov.getIdProveedor();
    }

    public void update(proveedor prov) throws Exception{
        //Generate the call to the procedure
        String query="{CALL updateProveedor(?,?,?,?,?,?,?,?,?)}";    //Proveedor data
        //Generate the connection and open it
        conexionMySQL connMySQL = new conexionMySQL();
        Connection conn = connMySQL.open();
        //Generate the statement for query
        CallableStatement cstmt = conn.prepareCall(query);
        //Fill Admin data according to the procedure parameters.
        cstmt.setInt(1, prov.getIdProveedor());
        cstmt.setString(2, prov.getCompanyName());
        cstmt.setString(3, prov.getStreet());
        cstmt.setString(4, prov.getNum());
        cstmt.setString(5, prov.getSuburb());
        cstmt.setString(6, prov.getTel());
        cstmt.setString(7, prov.getManagerName());
        cstmt.setString(8, prov.getManagerLastNamep());
        cstmt.setString(9, prov.getManagerLastNamem());
        
        //Execute the procedure.
        cstmt.execute();
        //Close objects
        cstmt.close();
        connMySQL.close();
    }

    public void delete(int idProveedor) throws Exception{
        //Declare SQL query.
        String query = "UPDATE proveedor SET estatus = 0 WHERE idProveedor=" + idProveedor;
        //Generate db object and start conexion.
        conexionMySQL connMySQL = new conexionMySQL();
        Connection conn = connMySQL.open();
        //Generate the statement for query.
        Statement stmt = conn.createStatement();
        //Execute.
        stmt.executeUpdate(query);
        //Close objects.
        stmt.close();
        connMySQL.close();
        return;
    }

    public List<proveedor> getAll() throws Exception{
        //Declare SQL query.
        String query = "SELECT * FROM proveedor WHERE estatus = 1";
        //Generate db object and start conexion.
        conexionMySQL connMySQL = new conexionMySQL();
        Connection conn = connMySQL.open();
        //Generate the statement for query
        PreparedStatement pstmt = conn.prepareStatement(query);
        //Execute
        ResultSet rs = pstmt.executeQuery();
        //Declare list for result
        List<proveedor> provList = new ArrayList<>();

        while(rs.next()){
            proveedor prov = new proveedor();
            prov.setIdProveedor(rs.getInt("idProveedor"));
            prov.setCompanyName(rs.getString("companyName"));
            prov.setStreet(rs.getString("street"));
            prov.setNum(rs.getString("num"));
            prov.setSuburb(rs.getString("suburb"));
            prov.setTel(rs.getString("tel"));
            prov.setManagerName(rs.getString("managerName"));
            prov.setManagerLastNamep(rs.getString("managerLastNamep"));
            prov.setManagerLastNamem(rs.getString("managerLastNamem"));
            provList.add(prov);
        }
        return provList;
    }
}
