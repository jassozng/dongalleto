package com.utl.idgs.dongalleto.demo.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class conexionMySQL {
    Connection conn;
    public Connection open() throws Exception{
        String user="root";
        String password="dBBnz9Z76E9E";
        String url="jdbc:mysql://localhost:3306/dongalleto?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=GMT&useSSL=false&allowPublicKeyRetrieval=true";
        //String url = "//localhost:3306/applab?autoReconnect=true&useSSL=false";
        Class.forName("com.mysql.cj.jdbc.Driver");
        conn=DriverManager.getConnection(url, user, password);
        return conn;    
    }
    
    public void close(){
        try {
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
