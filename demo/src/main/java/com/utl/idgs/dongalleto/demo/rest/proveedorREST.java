package com.utl.idgs.dongalleto.demo.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;

import com.utl.idgs.dongalleto.demo.controller.controllerProveedor;
import com.utl.idgs.dongalleto.demo.model.proveedor;
import com.google.gson.Gson;

@RestController
@RequestMapping("/rest/proveedor")
public class proveedorREST {
    @PostMapping("/insert")
    @ResponseBody
    public String insert(@FormParam("idProveedor") Integer idProveedor,
                         @FormParam("companyName") @DefaultValue("null") String companyName,
                         @FormParam("street") @DefaultValue("null") String street,
                         @FormParam("num") @DefaultValue("null") String num,
                         @FormParam("suburb") @DefaultValue("null") String suburb,
                         @FormParam("tel") @DefaultValue("null") String tel,
                         @FormParam("managerName") @DefaultValue("null") String managerName,
                         @FormParam("managerLastNamep") @DefaultValue("null") String managerLastNamep,
                         @FormParam("managerLastNamem") @DefaultValue("null") String managerLastNamem) throws Exception {
        String out = "";
        proveedor prov = new proveedor(idProveedor, companyName, street, num, suburb, tel, managerName, managerLastNamep, managerLastNamem);
        controllerProveedor ctrlProv = new controllerProveedor();
        try {
            if(idProveedor == 0){
                ctrlProv.insert(prov);
                out="{\"response\": \"Proveedor Registrado\"}";
            }else{
                ctrlProv.update(prov);
                out="{\"response\": \"Proveedor Actualizado\"}";
            }
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"exception\":\" "+e.toString()+" \"}";
        }
        return out;
    }

    @GetMapping("/getAll")
    @ResponseBody
    public String getAll() throws Exception{
        String out="";
        controllerProveedor ctrlProv = new controllerProveedor();
        try{
            List<proveedor> proveedorList = new ArrayList<>();
            proveedorList=ctrlProv.getAll();
            if(!proveedorList.isEmpty())
                out=new Gson().toJson(proveedorList);
            else{
                out="{\"exception\":\"No data\"}";
            }
        }
        catch(Exception e){
            e.printStackTrace();
            out="{\"exception\":\""+e.toString()+"\"}";
        }
        return out;
    }

    @PostMapping("/delete")
    @ResponseBody
    public String delete(@FormParam("idProveedor") @DefaultValue("") Integer idProveedor) throws Exception{
        
        String out="";
        controllerProveedor ctrlProv = new controllerProveedor();
        try{
            ctrlProv.delete(idProveedor);
            out="{\"response\":\"Proveedor Eliminado\"}";
        }
        catch(Exception e){
            e.printStackTrace();
            out="{\"exception\":\""+e.toString()+"\"}";
        }
        return out;
    }
}
