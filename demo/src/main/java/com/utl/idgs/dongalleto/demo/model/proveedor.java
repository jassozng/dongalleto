package com.utl.idgs.dongalleto.demo.model;

public class proveedor {
    int idProveedor;
    String companyName;
    String street;
    String num;
    String suburb;
    String tel;
    String managerName;
    String managerLastNamep;
    String managerLastNamem;

    public proveedor(){};

    public proveedor(int idProveedor, String companyName, String street, String num, String suburb, String tel, String managerName, String managerLastNamep, String managerLastNamem) {
        this.idProveedor = idProveedor;
        this.companyName = companyName;
        this.street = street;
        this.num = num;
        this.suburb = suburb;
        this.tel = tel;
        this.managerName = managerName;
        this.managerLastNamep = managerLastNamep;
        this.managerLastNamem = managerLastNamem;
    }

    public int getIdProveedor() {
        return this.idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getCompanyName() {
        return this.companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStreet() {
        return this.street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNum() {
        return this.num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getSuburb() {
        return this.suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getTel() {
        return this.tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getManagerName() {
        return this.managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerLastNamep() {
        return this.managerLastNamep;
    }

    public void setManagerLastNamep(String managerLastNamep) {
        this.managerLastNamep = managerLastNamep;
    }

    public String getManagerLastNamem() {
        return this.managerLastNamem;
    }

    public void setManagerLastNamem(String managerLastNamem) {
        this.managerLastNamem = managerLastNamem;
    }
}
